#include <iostream>
#include <cstdint>
#include <cstdlib>
#include <cstdio>

#include <string>
#include <vector>

#include <ctime>
#include <unistd.h>

#include <chrono>

#include <serial_helper.hpp>

#define DEBUG 1

using std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using std::chrono::duration;
using std::chrono::milliseconds;

serial_helper iSerial;
int64_t last_nsec;

auto t1 = high_resolution_clock::now();
auto t2 = high_resolution_clock::now();

std::string tambahan = "terserah";

int _msleep(uint32_t ms){
	return usleep(ms * 1000);
}

void listener_handler(std::string response){
	tambahan = "tambahan | " + response;
	//~ std::cout << "WAKAKAK: " << tambahan << std::endl;
}

void listener_handler_bin(std::vector<uint8_t> bResponse){
	auto diff = duration_cast<std::chrono::microseconds>(high_resolution_clock::now() - t2);
	for(int i = 0; i < bResponse.size(); i++){
		printf("byte-%i: 0x%0x\n", i, bResponse[i]);
	}
	//~ printf("-----------------------------------------\n");
	std::cout << diff << " " << bResponse.size() << std::endl;
	t2 = high_resolution_clock::now();
}

int main(int argc, char *argv[]) {
	iSerial.pause = 2000;
	iSerial.timeout = 10;

	iSerial.open("/dev/ttyUSB0", 2000000);
	//~ iSerial.attach_handler(listener_handler);
	iSerial.attach_handler_bin(listener_handler_bin);

	std::vector<uint8_t> bCmd;
	bCmd.push_back(0x70); // p
	bCmd.push_back(0x68); // h
	bCmd.push_back(0x72); // r
	bCmd.push_back(0x61); // a
	bCmd.push_back(0x73); // s
	bCmd.push_back(0x66); // e
	bCmd.push_back(0x0a); // \n

	std::string response;

	t2 = high_resolution_clock::now();
	while(1){
		//~ response = iSerial.request("setADC\n");
		//~ std::cout << response << std::endl;

		std::vector<uint8_t> bResponse = iSerial.request_bin(bCmd);
		//~ for(int i = 0; i < bResponse.size(); i++){
			//~ printf("0x%0x ", bResponse[i], bResponse[i]);
		//~ }
		//~ printf("\n");
		//~ std::cout << "WAKAKAK: " << tambahan << std::endl;

		usleep(5000);
	}

	exit(0);
}
