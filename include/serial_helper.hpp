#ifndef _SERIAL_HELPER_H_
#define _SERIAL_HELPER_H_

#include <string>
#include <vector>
#include <functional>

#include <sigc++/sigc++.h>

class serial_helper {
	public:
		serial_helper();
		void open(std::string, uint32_t);
		void close();
		std::string request(std::string);
		std::vector<uint8_t> request_bin(std::vector<uint8_t>);
		void attach_handler(void (*func)(std::string));
		void attach_handler_bin(void (*func)(std::vector<uint8_t>));

		std::string port;
		uint32_t baud;
		uint32_t pause;
		uint32_t timeout;

	private:
		bool wait_for_serialport();
		sigc::signal<void(std::string)> signal_response;
		sigc::signal<void(std::vector<uint8_t>)> signal_response_bin;
		uint8_t attached_signal_handler;
};

#endif
