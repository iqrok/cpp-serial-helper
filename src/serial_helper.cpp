#include "serial_helper.hpp"

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

#include <CppLinuxSerial/SerialPort.hpp>

#define SERIALPORT_FIND_SLEEP_US 50000

#define HANDLER_BIT_BIN 1
#define HANDLER_BIT_ASCII 0

mn::CppLinuxSerial::SerialPort serialPort;

uint8_t extract_attachment_status(uint8_t byte, uint8_t bit){
	return ( (byte & (1 << bit)) >> bit ) & 1;
}

void set_attachment_status(uint8_t *byte, uint8_t bit){
	*byte = (*byte) | (1 << bit);
}

bool file_exists( const char * Filename ){
    return access( Filename, 0 ) == 0;
}

serial_helper::serial_helper(){
	pause = 0;
	timeout = -1;
	attached_signal_handler = 0;
}

void serial_helper::close(){
	serialPort.Close();
}

void serial_helper::open(std::string _port, uint32_t _baud){
	port = _port;
	baud = _baud;

	if(!wait_for_serialport()){
		std::cerr << "Port " << port << " is unavailable!" << std::endl;
		exit(1);
	}

	serialPort.SetDevice(port);
	serialPort.SetBaudRate(baud);
	serialPort.SetTimeout(timeout);

	serialPort.Open();

	usleep(pause * 1000);
}

std::string serial_helper::request(std::string cmd){
	serialPort.Write(cmd);

	std::string readData;
	serialPort.Read(readData);

	if(extract_attachment_status(attached_signal_handler, HANDLER_BIT_ASCII)){
		signal_response.emit(readData);
	}

	return readData;
}

void serial_helper::attach_handler(void (*func)(std::string)){
	signal_response.connect(sigc::ptr_fun(func));
	set_attachment_status( &attached_signal_handler, HANDLER_BIT_ASCII );
}

std::vector<uint8_t> serial_helper::request_bin(std::vector<uint8_t> cmd){
	serialPort.WriteBinary(cmd);

	std::vector<uint8_t> readData;
	serialPort.ReadBinary(readData);

	if(extract_attachment_status(attached_signal_handler, HANDLER_BIT_BIN)){
		signal_response_bin.emit(readData);
	}

	return readData;
}

void serial_helper::attach_handler_bin(void (*func)(std::vector<uint8_t> response)){
	signal_response_bin.connect(sigc::ptr_fun(func));
	set_attachment_status( &attached_signal_handler, HANDLER_BIT_BIN );
}

bool serial_helper::wait_for_serialport(){
	uint16_t counter = 0;

	// loop until serial port exists
	while(!file_exists(port.c_str())){
		usleep(SERIALPORT_FIND_SLEEP_US);

		// timeout handling, so infinite loop wouldn't happen
		if(counter++ > 150){
			return false;
		}
	}

	return true;
}
